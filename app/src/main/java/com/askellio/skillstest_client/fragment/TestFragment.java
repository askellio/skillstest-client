package com.askellio.skillstest_client.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.activity.MainActivity;
import com.askellio.skillstest_client.model.Question;
import com.askellio.skillstest_client.presenter.TestPresenter;
import com.askellio.skillstest_client.util.GenericViewPagerAdapter;
import com.askellio.skillstest_client.view.TestView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by askellio on 10/1/17.
 */

public class TestFragment
        extends MvpAppCompatFragment
        implements TestView {


    @InjectPresenter
    TestPresenter presenter;

    private Unbinder unbinder;
    private View.OnClickListener listener;

    @BindView(R.id.test_pager)
    ViewPager pager;

    @BindView(R.id.test_spinner)
    ProgressBar progress;

    @ProvidePresenter
    TestPresenter provideTestPresenter () {
        return new TestPresenter(
                getArguments().getInt(
                        getString(R.string.tag_id)
                )
        );
    }

//    private GenericPagerAdapter<QuestionFragment> adapter;
    private GenericViewPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_test, container, false);
        unbinder = ButterKnife.bind(this, v);

        Bundle args = getArguments();
        if (args != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(
                    args.getString(
                            getString(R.string.tag_name)
                    )
            );
        }

        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox chbox = (CheckBox) v;
                presenter.setChecked (
                        pager.getCurrentItem(),
                        chbox.getText().toString(),
                        chbox.isChecked()
                );
            }
        };

//        adapter = new GenericPagerAdapter<>(getChildFragmentManager());
        adapter = new GenericViewPagerAdapter (getContext(), presenter, listener);
        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                presenter.setIndex (position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        return v;
    }

    @Override
    public void showData(List<Question> questions, int index) {
//        adapter.clear();

//        for (int i=0; i< questions.size(); i++) {
//            QuestionFragment fragment = new QuestionFragment();
//            Bundle args = new Bundle();
//            args.putInt(
//                    getString(R.string.tag_id),
//                    i
//            );
//            fragment.setArguments(args);
//            fragment.setPresenter (presenter);
//            fragment.setListener(listener);
//            adapter.addFragment(fragment);
//        }
//        adapter.notifyDataSetChanged();
//        pager.setCurrentItem(index);

        adapter.notifyDataSetChanged();
        pager.setCurrentItem(index);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(getView(), getString(resId), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        progress.setVisibility(
                isLoading ? View.VISIBLE : View.GONE
        );

        pager.setVisibility(
                isLoading ? View.GONE : View.VISIBLE
        );
    }

    @Override
    public void setIndex(int index) {
        if (index < pager.getChildCount())
            pager.setCurrentItem(index);
    }

    @OnClick(R.id.finish)
    public void onFinish () {
        presenter.finish();
    }

    @Override
    public void open_results() {
        FragmentManager manager = getFragmentManager();
        manager.popBackStackImmediate(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        manager
                .beginTransaction()
                .replace(R.id.container, new ResultsFragment())
                .commit();
    }
}
