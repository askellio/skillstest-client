package com.askellio.skillstest_client.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.activity.MainActivity;
import com.askellio.skillstest_client.model.Result;
import com.askellio.skillstest_client.presenter.ResultsPresenter;
import com.askellio.skillstest_client.util.GenericAdapter;
import com.askellio.skillstest_client.view.ResultsView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by askellio on 9/16/17.
 */

public class ResultsFragment
        extends MvpAppCompatFragment
        implements ResultsView{

    @InjectPresenter
    ResultsPresenter presenter;

    @BindView(R.id.ResultsRV)
    RecyclerView rv;

    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout layout;

    private Unbinder unbinder;
    private GenericAdapter<Result, ResultsPresenter> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_results, container, false);
        unbinder = ButterKnife.bind(this, v);

        LinearLayoutManager manager = new LinearLayoutManager(getContext());

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(
                getContext().getString(R.string.title_results)
        );

        adapter = new GenericAdapter<>(R.layout.item_result, presenter);
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);

        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.updateData();
            }
        });

        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(getView(), getString(resId), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        layout.setRefreshing(isLoading);
    }

    @Override
    public void showData() {
        adapter.update();
    }
}
