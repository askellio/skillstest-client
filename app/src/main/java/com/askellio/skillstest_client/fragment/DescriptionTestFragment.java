package com.askellio.skillstest_client.fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.askellio.skillstest_client.BR;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.activity.MainActivity;
import com.askellio.skillstest_client.model.Test;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by develop32 on 22.09.17.
 */

public class DescriptionTestFragment extends Fragment{

    private Unbinder unbinder;
    private ViewDataBinding binding;
    private int id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_decription_test, container, false);
        binding = DataBindingUtil.bind(v);
        unbinder = ButterKnife.bind(this, v);
        Test test = null;
        Bundle args = getArguments();
        if (args != null)
            test = args.getParcelable(
                    getString(R.string.tag_item)
            );

        id = -1;
        if (test != null) {
            id = test.getId();
            binding.setVariable(BR.item, test);
            binding.executePendingBindings();

            ((MainActivity) getActivity()).getSupportActionBar().setTitle(test.getName());
        }

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.unbind();
        unbinder.unbind();
    }

    @OnClick(R.id.startBtn)
    public void startTest() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = new TestFragment();

        Bundle args = new Bundle();
        args.putInt(
                getString(R.string.tag_id),
                id
        );
        args.putString(
                getString(R.string.tag_name),
                ((MainActivity) getActivity()).getSupportActionBar().getTitle().toString()
        );

        fragment.setArguments(args);

        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
