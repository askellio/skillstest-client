package com.askellio.skillstest_client.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.activity.MainActivity;
import com.askellio.skillstest_client.model.Category;
import com.askellio.skillstest_client.presenter.CategoryPresenter;
import com.askellio.skillstest_client.util.GenericAdapter;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.askellio.skillstest_client.view.CategoryView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by askellio on 9/16/17.
 */

public class CategoryFragment
        extends MvpAppCompatFragment
        implements CategoryView{

    @InjectPresenter
    CategoryPresenter presenter;

    @BindView(R.id.CategoriesRV)
    RecyclerView rv;

    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout layout;

    private Unbinder unbinder;
    private GenericAdapter<Category, CategoryPresenter> adapter;

    @ProvidePresenter
    CategoryPresenter provideCategoryPresenter () {
        if (getArguments() != null)
            return new CategoryPresenter(
                getArguments().getInt(
                        getString(R.string.tag_id),
                        SkillsTestApplication.getIntRes(R.integer.default_category)
                )
            );
        else
            return new CategoryPresenter(
                    SkillsTestApplication.getIntRes(R.integer.default_category)
            );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frargment_categories, container, false);
        unbinder = ButterKnife.bind(this, v);

        Bundle args = getArguments();
        String name = null;
        if (args != null) {
            name = args.getString(
                    SkillsTestApplication.getStringRes(R.string.tag_name),
                    null
            );
        }

        if (name != null)
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(name);
        else
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_categories));
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        adapter = new GenericAdapter<>(R.layout.item_category, presenter);
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);

        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.updateData();
            }
        });

        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(getView(), getString(resId), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        layout.setRefreshing(isLoading);
    }

    @Override
    public void showData() {
        adapter.update();
    }


    @Override
    public void openCategory(int categoryId, String name) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt(
                SkillsTestApplication.getStringRes(R.string.tag_id),
                categoryId
        );
        args.putString(
                SkillsTestApplication.getStringRes(R.string.tag_name),
                name
        );

        fragment.setArguments(args);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void openTest(int testId) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = new ListTestFragment();
        Bundle args = new Bundle();
        args.putInt(
                SkillsTestApplication.getStringRes(R.string.tag_id),
                testId
        );

        fragment.setArguments(args);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
