package com.askellio.skillstest_client.fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.askellio.skillstest_client.BR;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.model.Question;
import com.askellio.skillstest_client.model.Variant;
import com.askellio.skillstest_client.presenter.TestPresenter;

/**
 * Created by askellio on 9/16/17.
 */

public class QuestionFragment extends MvpAppCompatFragment {

    private Question question;
    private TestPresenter presenter;

    private ViewDataBinding binding;
    private View.OnClickListener listener;

    private int position;
//    private Unbinder unbinder;

//
//    @BindView(R.id.variant_layout)
//    LinearLayout layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_question, container, false);
//        unbinder = ButterKnife.bind(this, v);
        Bundle args = getArguments();
        if (args != null) {
            int pos = args.getInt(
                    getString(R.string.tag_id)
            );

            position = pos;

            if (presenter != null)
                question = presenter.getQuestion(position);
            binding = DataBindingUtil.bind(v);
            binding.setVariable(BR.item, question);
            binding.setVariable(BR.presenter, presenter);
            binding.executePendingBindings();

            LinearLayout layout = (LinearLayout) v.findViewById(R.id.variant_layout);
            if (question != null)
            for (Variant variant: question.getVariants()) {
                CheckBox chbox = new CheckBox(getContext());
                chbox.setText(variant.getValue());
                chbox.setChecked(
                        presenter.isChecked (position, variant)
                );
                if (listener != null)
                    chbox.setOnClickListener(listener);
                layout.addView(
                        chbox,
                        new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                );
            }

        }

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
        binding.unbind();
    }

    public void setPresenter (TestPresenter presenter) {
        this.presenter = presenter;
    }

    public void setListener (View.OnClickListener listener) {
        this.listener = listener;
    }
}
