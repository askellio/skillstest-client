package com.askellio.skillstest_client.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.activity.MainActivity;
import com.askellio.skillstest_client.model.Test;
import com.askellio.skillstest_client.presenter.ListTestPresenter;
import com.askellio.skillstest_client.util.GenericAdapter;
import com.askellio.skillstest_client.util.Util;
import com.askellio.skillstest_client.view.ListTestView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by askellio on 9/16/17.
 */

public class ListTestFragment extends MvpAppCompatFragment implements ListTestView {

    @InjectPresenter
    ListTestPresenter presenter;

    @BindView(R.id.TestsRV)
    RecyclerView rv;

    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout layout;

    private Unbinder unbinder;
    private GenericAdapter<Test, ListTestPresenter> adapter;

    @ProvidePresenter
    ListTestPresenter provideTestPresenter () {
        return new ListTestPresenter(
                getArguments().getInt(
                        getString(R.string.tag_id)
                )
        );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_test, container, false);
        unbinder = ButterKnife.bind(this, v);

        getActivity().setTitle(getString(R.string.title_tests));

        GridLayoutManager manager = new GridLayoutManager(
                getContext(),
                Util.calculateNoOfColumns(getContext())
        );

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(
                getContext().getString(R.string.title_tests)
        );

        adapter = new GenericAdapter<>(R.layout.item_test, presenter);
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);

        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.updateData();
            }
        });

        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(getView(), getString(resId), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        layout.setRefreshing(isLoading);
    }

    @Override
    public void showData() {
        adapter.update();
    }

    @Override
    public void openTest(Test test) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = new DescriptionTestFragment();

        Bundle args = new Bundle();
        args.putParcelable(
                getString(R.string.tag_item),
                test
        );

        fragment.setArguments(args);

        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
