package com.askellio.skillstest_client.presenter;

import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.util.RestApi;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.askellio.skillstest_client.view.LoginView;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by askellio on 9/16/17.
 */

@InjectViewState
public class LoginPresenter
        extends MvpPresenter<LoginView> {

    @Inject
    RestApi api;

    @Inject
    SharedPreferences preferences;

    public LoginPresenter() {
        super();
        SkillsTestApplication.getNetComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        api.wakeUpServer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull Response<String> response) {

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }

    public void onAuth(String login, String password) {

        if (login.isEmpty() || password.isEmpty()) {
            getViewState().showMessage(R.string.msg_input_all);
            return;
        }

        api.auth(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        getViewState().showLoading(true);
                    }

                    @Override
                    public void onSuccess(@NonNull Response<String> response) {
                        getViewState().showLoading(false);
                        if (response.code() == 200) {
                            preferences.edit().putString(
                                    SkillsTestApplication.getStringRes(R.string.cookie),
                                    response.body()
                            ).apply();
                            getViewState().switchActivity();
                        } else
                            getViewState().showMessage("Неверный логин пароль");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showLoading(false);




                    }
                });
    }
}
