package com.askellio.skillstest_client.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.model.Test;
import com.askellio.skillstest_client.util.AdapterPresenter;
import com.askellio.skillstest_client.util.RestApi;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.askellio.skillstest_client.util.Util;
import com.askellio.skillstest_client.view.ListTestView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by askellio on 9/16/17.
 */

@InjectViewState
public class ListTestPresenter
        extends MvpPresenter<ListTestView>
        implements AdapterPresenter<Test> {

    @Inject
    RestApi api;

    private int categoryId;
    private List<Test> tests;

    public ListTestPresenter(int categoryId) {
        super();
        this.categoryId = categoryId;
        tests = new ArrayList<>();

        SkillsTestApplication.getNetComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        updateData();
    }

    public void updateData () {

        api.getTests(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<List<Test>>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        getViewState().showLoading(true);
                    }

                    @Override
                    public void onSuccess(@NonNull Response<List<Test>> response) {
                        getViewState().showLoading(false);
                        if (response != null ) {
                            if (response.isSuccessful()) {
                                tests = response.body();
                                if (tests != null && tests.size() > 0)
                                    getViewState().showData();
                                else
                                    getViewState().showMessage(SkillsTestApplication.getStringRes(R.string.data_empty));
                            } else
                                getViewState().showMessage("Ошибка "+response.code());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showLoading(false);
                        String message = Util.checkException(e);
                        getViewState().showMessage(message);
                    }
                });
    }

    @Override
    public int getItemsCount() {
        return tests.size();
    }

    @Override
    public Test getItem(int position) {
        return tests.get(position);
    }

    public void onClickItem (Test test) {
        getViewState().openTest(test);
//        int id = category.getId();
//        if (category.getChilds() > 0)
//            getViewState().openCategory(id, category.getName());
//        else
//            getViewState().openTest(id);
    }
}
