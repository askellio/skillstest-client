package com.askellio.skillstest_client.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.model.Question;
import com.askellio.skillstest_client.model.Variant;
import com.askellio.skillstest_client.util.AdapterPresenter;
import com.askellio.skillstest_client.util.RestApi;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.askellio.skillstest_client.util.Util;
import com.askellio.skillstest_client.view.TestView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by askellio on 10/1/17.
 */

@InjectViewState
public class TestPresenter
        extends MvpPresenter<TestView>
        implements AdapterPresenter{

    @Inject
    RestApi api;

    private int id;
    private List<Question> questions;
    private List<Question> answers;
    private int index;

    public TestPresenter(int id) {
        super();
        this.id = id;
        index = 0;
        questions = new ArrayList<>();
        answers = new ArrayList<>();
        SkillsTestApplication.getNetComponent().inject(this);
    }

    public void setIndex (int index) {
        this.index = index;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        api.startTest(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<List<Question>>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        getViewState().showLoading(true);
                    }

                    @Override
                    public void onSuccess(@NonNull Response<List<Question>> response) {

                        getViewState().showLoading(false);
                        if (response != null ) {
                            if (response.isSuccessful()) {
                                questions = response.body();
                                if (questions != null && questions.size() > 0) {
                                    answers = new ArrayList<Question>();
                                    for (Question question: questions) {
                                        Question q = new Question(question);
                                        q.setVariants(new ArrayList<Variant>());
                                        answers.add(q);
                                    }
                                    getViewState().showData(questions, index);
                                } else
                                    getViewState().showMessage(SkillsTestApplication.getStringRes(R.string.data_empty));
                            } else
                                getViewState().showMessage("Ошибка "+response.code());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showLoading(false);
                        String message = Util.checkException(e);
                        getViewState().showMessage(message);
                    }
                });
    }

    @Override
    public void attachView(TestView view) {
        super.attachView(view);
        getViewState().setIndex (index);
    }

    public void setChecked (int position, String text, boolean checked) {
        if (checked) {
            for (Variant variant: questions.get(position).getVariants()) {
                if (variant.getValue().compareTo(text) == 0) {
                    answers.get(position).getVariants().add(
                            variant
                    );
                    break;
                }
            }
        } else {
            for (Variant variant: answers.get(position).getVariants())
                if (variant.getValue().compareTo(text) == 0)
                    answers.get(position).getVariants().remove(variant);
        }

    }

    public boolean isChecked (int position, Variant variant) {
        return (answers.get(position).getVariants().indexOf(variant) >= 0);
    }

    public Question getQuestion (int position) {
        return questions.get(position);
    }

    @Override
    public int getItemsCount() {
        return questions.size();
    }

    @Override
    public Object getItem(int position) {
        return questions.get(position);
    }

    public void finish () {
        List<Integer> ids = new ArrayList<>();
        for (Question answer: answers)
            for (Variant variant: answer.getVariants())
                ids.add(variant.getId());

        api.finishTest(id, ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        getViewState().showLoading(true);
                    }

                    @Override
                    public void onSuccess(@NonNull Response<ResponseBody> response) {
                        getViewState().showLoading(false);
                        if (response != null ) {
                            if (response.isSuccessful()) {
                                getViewState().showMessage(SkillsTestApplication.getStringRes(R.string.result_saved));
                                getViewState().open_results();
                            } else
                                getViewState().showMessage("Ошибка "+response.code());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showLoading(false);
                        String message = Util.checkException(e);
                        getViewState().showMessage(message);
                    }
                });
    }
}
