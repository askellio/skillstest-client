package com.askellio.skillstest_client.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.model.Result;
import com.askellio.skillstest_client.util.AdapterPresenter;
import com.askellio.skillstest_client.util.RestApi;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.askellio.skillstest_client.util.Util;
import com.askellio.skillstest_client.view.ResultsView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by askellio on 9/16/17.
 */

@InjectViewState
public class ResultsPresenter
        extends MvpPresenter<ResultsView>
        implements AdapterPresenter<Result> {

    @Inject
    RestApi api;

    private List<Result> results;

    public ResultsPresenter() {
        super();
        results = new ArrayList<>();
        SkillsTestApplication.getNetComponent().inject (this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        updateData();
    }

    public void updateData () {

//        api.getCategories(cookie, id)
        api.getResults()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<List<Result>>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        getViewState().showLoading(true);
                    }

                    @Override
                    public void onSuccess(@NonNull Response<List<Result>> response) {
                        getViewState().showLoading(false);
                        if (response != null ) {
                            if (response.isSuccessful()) {
                                results = response.body();
                                if (results != null && results.size() > 0)
                                    getViewState().showData();
                                else
                                    getViewState().showMessage(SkillsTestApplication.getStringRes(R.string.data_empty));
                            } else
                                getViewState().showMessage("Ошибка "+response.code());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showLoading(false);
                        String message = Util.checkException(e);
                        getViewState().showMessage(message);
                    }
                });
    }

    @Override
    public int getItemsCount() {
        return results.size();
    }

    @Override
    public Result getItem(int position) {
        return results.get(position);
    }
}
