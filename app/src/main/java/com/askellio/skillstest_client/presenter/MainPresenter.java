package com.askellio.skillstest_client.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.askellio.skillstest_client.view.MainView;

/**
 * Created by askellio on 9/16/17.
 */

@InjectViewState
public class MainPresenter
        extends MvpPresenter<MainView> {

    public void BackStackChanged (int count) {
        if (count > 0) {
            getViewState().setDrawerIndicatorEnabled(false);
        } else
            getViewState().setDrawerIndicatorEnabled(true);
    }
}
