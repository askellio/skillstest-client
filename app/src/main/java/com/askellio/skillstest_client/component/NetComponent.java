package com.askellio.skillstest_client.component;

import com.askellio.skillstest_client.activity.LoginActivity;
import com.askellio.skillstest_client.module.AppModule;
import com.askellio.skillstest_client.module.NetModule;
import com.askellio.skillstest_client.module.PreferencesModule;
import com.askellio.skillstest_client.presenter.CategoryPresenter;
import com.askellio.skillstest_client.presenter.LoginPresenter;
import com.askellio.skillstest_client.presenter.ResultsPresenter;
import com.askellio.skillstest_client.presenter.ListTestPresenter;
import com.askellio.skillstest_client.presenter.TestPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by askellio on 9/17/17.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class, PreferencesModule.class})
public interface NetComponent {
    void inject(LoginActivity activity);
    void inject(LoginPresenter presenter);
    void inject(CategoryPresenter presenter);
    void inject(ListTestPresenter presenter);
    void inject(ResultsPresenter presenter);
    void inject(NetModule.CookieInterceptor interceptor);
    void inject(TestPresenter presenter);
}
