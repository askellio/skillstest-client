package com.askellio.skillstest_client.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.askellio.skillstest_client.R;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by askellio on 9/21/17.
 */

@Module
public class PreferencesModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefernces (Application application) {
        return application.getSharedPreferences(
                application.getString(R.string.settings),
                Context.MODE_PRIVATE
        );
    }
}
