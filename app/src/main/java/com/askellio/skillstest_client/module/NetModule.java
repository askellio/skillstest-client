package com.askellio.skillstest_client.module;

import android.app.Application;
import android.content.SharedPreferences;

import com.askellio.skillstest_client.util.RestApi;
import com.askellio.skillstest_client.util.SkillsTestApplication;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by askellio on 9/17/17.
 */

@Module
public class NetModule {
    String baseUrl;

    public NetModule(String mBaseUrl) {
        this.baseUrl = mBaseUrl;
    }


    @Provides
    @Singleton
    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.setLenient();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        client.addInterceptor(logging);
        client.addInterceptor(new CookieInterceptor());
        return client.build();
    }

    @Provides
    @Singleton
    RestApi provideAPi(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
                .create(RestApi.class);
    }

    public class CookieInterceptor implements Interceptor {

        @Inject
        SharedPreferences preferences;

        public CookieInterceptor() {
            SkillsTestApplication.getNetComponent().inject(this);
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            String cookie = preferences.getString("cookie", "");

            Request request = original.newBuilder()
                    .headers(original.headers())
                    .header("cookie", cookie)
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    }
}