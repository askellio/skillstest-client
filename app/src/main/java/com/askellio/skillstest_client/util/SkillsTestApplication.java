package com.askellio.skillstest_client.util;

import android.app.Application;
import android.content.Context;

import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.component.DaggerNetComponent;
import com.askellio.skillstest_client.component.NetComponent;
import com.askellio.skillstest_client.module.AppModule;
import com.askellio.skillstest_client.module.NetModule;

/**
 * Created by askellio on 9/17/17.
 */

public class SkillsTestApplication extends Application {

    private static NetComponent net;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        net = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(
                        getResources()
                            .getString(R.string.base_url)
                ))
                .build();
    }

    public static NetComponent getNetComponent() {
        return net;
    }

    public static String getStringRes (int resId) {
        return (context.getString(resId));
    }

    public static int getIntRes (int resId) {
        return (context.getResources().getInteger(resId));
    }
}
