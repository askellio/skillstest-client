package com.askellio.skillstest_client.util;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.askellio.skillstest_client.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * Created by askellio on 9/22/17.
 */

public class Util {
    public static String checkException (Throwable e) {
        if (e instanceof HttpException)
            return ("Ошибка "+((HttpException) e).code());
        else {
            if (e instanceof IOException)
                return (SkillsTestApplication.getStringRes(R.string.msg_check_net_connection));
            else
                return (e.getMessage());
        }
    }

    @BindingAdapter(value = {"image"}, requireAll = true)
    public static void showImage (ImageView imageView,
                                  String image) {

        Glide.with(imageView.getContext())
                .load(image)
                .apply(new RequestOptions()
                        .centerInside()
                        .placeholder(R.drawable.load)
                        .circleCrop()
                        .error(R.drawable.no_image)
                )
                .into(imageView);
    }

    @BindingAdapter(value = {"circle"}, requireAll = true)
    public static void showImage (ImageView imageView,
                                  boolean circle) {


    }

    public static float calculateWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return dpWidth;
    }

    public static int calculateNoOfColumns(Context context) {
        float dpWidth = calculateWidth(context);
        return (int) (dpWidth / 180);
    }
}
