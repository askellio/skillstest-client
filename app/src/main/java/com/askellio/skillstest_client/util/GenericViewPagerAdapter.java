package com.askellio.skillstest_client.util;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.askellio.skillstest_client.BR;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.model.Question;
import com.askellio.skillstest_client.model.Variant;
import com.askellio.skillstest_client.presenter.TestPresenter;

/**
 * Created by askellio on 10/2/17.
 */

public class GenericViewPagerAdapter extends PagerAdapter  implements ViewPager.OnPageChangeListener{

    private TestPresenter presenter;
    private LayoutInflater inflater;
    private ViewDataBinding binding;
    private Context context;
    private View.OnClickListener listener;



    public GenericViewPagerAdapter(Context context,
                                   TestPresenter presenter,
                                   View.OnClickListener listener) {
        super();
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.presenter = presenter;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return presenter.getItemsCount();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.item_question, container, false);

        Question question = presenter.getQuestion(position);
        binding = DataBindingUtil.bind(v);
        binding.setVariable(BR.item, question);
        binding.setVariable(BR.presenter, presenter);
        binding.executePendingBindings();

        LinearLayout layout = (LinearLayout) v.findViewById(R.id.variant_layout);

        for (Variant variant: question.getVariants()) {
            CheckBox chbox = new CheckBox(context);
            chbox.setText(variant.getValue());
            chbox.setChecked(
                    presenter.isChecked (position, variant)
            );
            if (listener != null)
                chbox.setOnClickListener(listener);
            layout.addView(
                    chbox,
                    new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    )
            );
        }

        container.addView(v);

        return v;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        presenter.setIndex(state);
    }
}
