package com.askellio.skillstest_client.util;

import com.askellio.skillstest_client.model.Category;
import com.askellio.skillstest_client.model.Question;
import com.askellio.skillstest_client.model.Result;
import com.askellio.skillstest_client.model.Test;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by askellio on 9/17/17.
 */

public interface RestApi  {

    @GET("api/good")
    Single<Response<String>> wakeUpServer ();

    @FormUrlEncoded
    @POST("api/categories")
    Single<Response<List<Category>>> getCategories(@Field("id") int id);

    @FormUrlEncoded
    @POST("api/login")
    Single<Response<String>> auth (@Field("login") String login, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/tests")
    Single<Response<List<Test>>> getTests(@Field("id") int id);

    @FormUrlEncoded
    @POST("api/start")
    Single<Response<List<Question>>> startTest(@Field("id") int id);

    @FormUrlEncoded
    @POST("api/finish")
    Single<Response<ResponseBody>> finishTest(@Field("id") int testId, @Field("answers") List<Integer> answers);

    @POST("api/results")
    Single<Response<List<Result>>> getResults ();
}
