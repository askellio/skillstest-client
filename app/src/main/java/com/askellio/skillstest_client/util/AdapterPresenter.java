package com.askellio.skillstest_client.util;

/**
 * Created by askellio on 9/17/17.
 */


public interface AdapterPresenter<T> {
    public int getItemsCount ();
    public T getItem (int position);
}