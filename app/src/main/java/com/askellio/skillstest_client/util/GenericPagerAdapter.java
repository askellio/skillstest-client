package com.askellio.skillstest_client.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by askellio on 10/1/17.
 */

public class GenericPagerAdapter<F extends Fragment> extends FragmentPagerAdapter {

    private List<F> items = new ArrayList<>();

    public GenericPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void addFragment (F frag) {
        items.add(frag);
    }


    public void clear() {
        if (items.size() > 0) {
            items.clear();
            notifyDataSetChanged();
        }
    }

}
