package com.askellio.skillstest_client.util;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.askellio.skillstest_client.BR;

/**
 * Created by askellio on 9/16/17.
 */

public class GenericAdapter<T, P extends AdapterPresenter<T>>
        extends RecyclerView.Adapter<GenericAdapter.ViewHolder>{

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        public ViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    private int layout;
    private P presenter;

    public GenericAdapter(int layout, P presenter) {
        this.layout = layout;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from (parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GenericAdapter.ViewHolder holder, int position) {
        final T item = presenter.getItem(position);
        holder.getBinding().setVariable(BR.item, item);
        holder.getBinding().setVariable(BR.presenter, presenter);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return presenter.getItemsCount();
    }

    public void update() {
        notifyDataSetChanged();
    }

    public void updateItem (int position) {
        notifyItemChanged(position);
    }
}