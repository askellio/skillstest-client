package com.askellio.skillstest_client.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.fragment.AboutFragment;
import com.askellio.skillstest_client.fragment.CategoryFragment;
import com.askellio.skillstest_client.fragment.ResultsFragment;
import com.askellio.skillstest_client.fragment.TestFragment;
import com.askellio.skillstest_client.presenter.MainPresenter;
import com.askellio.skillstest_client.view.MainView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends MvpAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MainView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Unbinder unbinder;
    private ActionBarDrawerToggle toggle;
    private View.OnClickListener originalToolbarListener;

    @InjectPresenter
    public MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        originalToolbarListener = toggle.getToolbarNavigationClickListener();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("d");


        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Fragment fragment = new CategoryFragment();
            Bundle args = new Bundle();
            args.putInt(
                    getString(R.string.tag_id),
                    getResources().getInteger(R.integer.default_category)
            );
            fragment.setArguments(args);
            transaction.replace(R.id.container, fragment)
                    .commit();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        presenter.BackStackChanged (
                                getSupportFragmentManager().getBackStackEntryCount()
                        );
                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                List<Fragment> fragments = manager.getFragments();
                int lastPos = fragments.size()-1;
                if (fragments.get(lastPos).getClass() == TestFragment.class)
                    moveTaskToBack(true);
                else
                    super.onBackPressed();
            } else {
                moveTaskToBack(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0)
            manager.popBackStackImmediate(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Fragment fragment = null;
        if (id == R.id.nav_categories) {
            fragment = new CategoryFragment();
        } else if (id == R.id.nav_results) {
            fragment = new ResultsFragment();
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
        }

        if (fragment != null)
            manager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
        
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setDrawerIndicatorEnabled(boolean flag) {
        toggle.setDrawerIndicatorEnabled(flag);
        if (!flag) {
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getSupportFragmentManager().popBackStack();
                }
            });
        } else {
            toggle.setToolbarNavigationClickListener(originalToolbarListener);
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showMessage(int resId) {

    }

    @Override
    public void showLoading(boolean isLoading) {

    }
}
