package com.askellio.skillstest_client.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.askellio.skillstest_client.R;
import com.askellio.skillstest_client.presenter.LoginPresenter;
import com.askellio.skillstest_client.view.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by askellio on 9/16/17.
 */

public class LoginActivity
        extends MvpAppCompatActivity
        implements LoginView {

    @InjectPresenter
    LoginPresenter presenter;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.login)
    EditText editLogin;

    @BindView(R.id.password)
    EditText editPassword;

    @BindView(R.id.authBtn)
    Button btnAuth;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
        setTitle(R.string.app_name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int resId) {
        Snackbar.make(findViewById(android.R.id.content), getString(resId), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            progress.setVisibility(View.VISIBLE);
            btnAuth.setVisibility(View.GONE);
        }
        else {
            progress.setVisibility(View.GONE);
            btnAuth.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void switchActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.authBtn)
    public void OnAuth() {
        presenter.onAuth(editLogin.getText().toString(),
                editPassword.getText().toString()
        );
    }
}
