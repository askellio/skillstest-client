package com.askellio.skillstest_client.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by askellio on 9/16/17.
 */

public interface CategoryView extends BaseView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    public void showData ();

    @StateStrategyType(SkipStrategy.class)
    public void openCategory (int categoryId, String name);

    @StateStrategyType(SkipStrategy.class)
    public void openTest (int testId);
}
