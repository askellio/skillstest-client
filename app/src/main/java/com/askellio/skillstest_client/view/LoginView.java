package com.askellio.skillstest_client.view;

/**
 * Created by askellio on 9/16/17.
 */


public interface LoginView  extends BaseView{

    void switchActivity();
}
