package com.askellio.skillstest_client.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.askellio.skillstest_client.model.Test;

/**
 * Created by askellio on 9/16/17.
 */

public interface ListTestView extends BaseView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    public void showData ();

    @StateStrategyType(SkipStrategy.class)
    public void openTest(Test test);
}
