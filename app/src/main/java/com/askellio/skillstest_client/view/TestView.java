package com.askellio.skillstest_client.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.askellio.skillstest_client.model.Question;

import java.util.List;

/**
 * Created by askellio on 10/1/17.
 */

public interface TestView extends BaseView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showData(List<Question> questions, int index);

    @StateStrategyType(SkipStrategy.class)
    void setIndex (int index);

    @StateStrategyType(SkipStrategy.class)
    void open_results();
}
