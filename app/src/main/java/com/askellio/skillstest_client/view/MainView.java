package com.askellio.skillstest_client.view;

/**
 * Created by askellio on 9/16/17.
 */

public interface MainView extends BaseView{

    public void setDrawerIndicatorEnabled(boolean enabled);
}
