package com.askellio.skillstest_client.view;

/**
 * Created by askellio on 9/16/17.
 */

public interface ResultsView extends BaseView {
    void showData();
}
