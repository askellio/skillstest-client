package com.askellio.skillstest_client.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by askellio on 9/20/17.
 */

public class Test implements Parcelable {
    private int id;
    private int count;
    private String image;
    private String name;
    private String description;
    private long duration;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {

        out.writeInt(getId());
        out.writeInt(getCount());
        out.writeString(getImage());
        out.writeString(getName());
        out.writeString(getDescription());
        out.writeLong(getDuration());
    }

    public static final Parcelable.Creator<Test> CREATOR =
            new Parcelable.Creator<Test>() {
                public Test createFromParcel (Parcel in) {
                    return new Test(in);
                }

                public Test[] newArray(int size) {
                    return new Test[size];
                }
            };
    private Test (Parcel in) {
        setId(in.readInt());
        setCount(in.readInt());
        setImage(in.readString());
        setName(in.readString());
        setDescription(in.readString());
        setDuration(in.readLong());
    }
}
