package com.askellio.skillstest_client.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by askellio on 9/20/17.
 */

public class Variant implements Parcelable {
    private int id;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(getId());
        out.writeString(getValue());
    }

    public static final Parcelable.Creator<Variant> CREATOR =
            new Parcelable.Creator<Variant>() {
                public Variant createFromParcel (Parcel in) {
                    return new Variant(in);
                }

                public Variant[] newArray(int size) {
                    return new Variant[size];
                }
            };
    private Variant (Parcel in) {
        setId(in.readInt());
        setValue(in.readString());
    }

    public Variant (Variant variant) {
        setId(variant.getId());
        setValue(variant.getValue());
    }

}
