package com.askellio.skillstest_client.model;

/**
 * Created by askellio on 9/20/17.
 */

public class Category {
    private int id;
    private int childs;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChilds() {
        return childs;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
