package com.askellio.skillstest_client.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by askellio on 9/20/17.
 */

public class Question implements Parcelable {
    private int id;
    private List<String> images;
    private String text;
    private List<Variant> variants;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(getId());
        out.writeList(getImages());
        out.writeString(getText());
        out.writeTypedList(getVariants());
    }

    public static final Parcelable.Creator<Question> CREATOR =
            new Parcelable.Creator<Question>() {
                public Question createFromParcel (Parcel in) {
                    return new Question(in);
                }

                public Question[] newArray(int size) {
                    return new Question[size];
                }
            };

    private Question (Parcel in) {
        setId(in.readInt());
        in.readList(images, String.class.getClassLoader());
        setText(in.readString());
        in.readTypedList(variants, Variant.CREATOR);
    }

    public Question (Question question) {

        setId(question.getId());
        setImages(new ArrayList<String>(question.getImages()));
        setText(question.getText());
        setVariants(new ArrayList<Variant>());
        for (Variant variant: question.getVariants())
            getVariants().add(new Variant(variant));
    }
}
